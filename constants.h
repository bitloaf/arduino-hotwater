#pragma once
#ifndef CONSTANTS_H
#define CONSTANTS_H

#ifdef __cplusplus

#include <WProgram.h>

/** State constants */
enum State {
  IDLE,
  HEAT,
  KEEP,
  ERROR
};

/** Error codes */
enum ErrorCode {
  TIMEOUT_HEATING,
  TEMPERATURE_RISE_TOO_LOW,
  TEMPERATURE_RISE_TOO_HIGH,
  TEMPERATURE_RISE_ZERO,
  NO_ERRORS
};


/***** Pin constants *****/
// Temperature sensor:
const int PIN_TEMPERATURE_SENSOR = A1;
// Temperature control potentiometer:
const int PIN_TEMPERATURE_CONTROL = A0;
// On/off switch:
const int PIN_POWER_BUTTON = 2;
// Status LED:
const int PIN_STATUS_INDICATOR = 13;
// Element control relay:
const int PIN_RELAY_ELEMENT = 12;


#endif // __cplusplus

#endif // CONSTANTS_H
