#pragma once
#ifndef DISPLAY_H
#define DISPLAY_H

#include <WProgram.h>

#include "constants.h"
#include "time.h"


class Display {
  public:
    Display();
    ~Display();
    
    /** Sets the status indicator high or low (LED) */
    void setStatusIndicator(int s);
    /** Starts a blinking display */
    void startBlink(int ms);
    /** Stops blinking */
    void stopBlink();
    /** Update the display. Needed inside loops in the main program */
    void update();
    /** Toggle status indicator */
    void toggleStatus();
    
  private:
    /** Time related functions */
    //Time m_t;
    /** Blink flag */
    boolean isBlink;
};
    

#endif // DISPLAY_H
