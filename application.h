#pragma once
#ifndef APPLICATION_H
#define APPLICATION_H

//#ifdef __cplusplus

#include <WProgram.h>
#include "config.h"
#include "constants.h"
#include "time.h"
#include "display.h"

class Application {
  
  public:
    /** Constructor */
    Application();
    /** Deconstructor */
    ~Application();
    
    /** Initialize */
    void init();
    
    /** Set up inputs and outputs */
    void setupIO();
    /** Set up serial communication */
    void setupSerial();
    
    /** Read the current temperature from input */
    int getTemperaturePWM();
    /** Read the current temperature control value from input */
    int getTemperatureControlPWM();
    /** Read the current temperature from input (converted to degrees C) */
    int getTemperatureValue();
    /** Read the current temperature control value from input (converted to percentage) */
    int getTemperatureControlValue();
    /** Checks the current power button value */
    boolean isPowerButtonOn();
    /** Checks the current power button value */
    boolean isPowerButtonOff();
    /** Checks the current power button value (true if on, false if off) */
    boolean getPowerButton();
    /** Runs infinite loop until power button is pressed ON */
    void waitForPowerButtonOn();
    /** Runs infinite loop until power button is pressed OFF */
    void waitForPowerButtonOff();
    
    /** Turns on heating element */
    void setElementPowerOn();
    /** Turns off heating element */
    void setElementPowerOff();
    /** Get current heating status */
    boolean getElementPowerOn();
    /** Sets program in cooldown */
    void setCooldown(boolean b);
    /** Checks if program is in cooldown */
    boolean isCooldown();
    
    /** Checks if element is heating */
    boolean isHeating();
    /** Checks if above MAX limit */
    boolean isAboveMaxTemperature();
    /** Checks if above MIN limit */
    boolean isAboveMinTemperature();
    /** Checks if below MAX limit */
    boolean isBelowMaxTemperature();
    /** Checks if below MIN limit */
    boolean isBelowMinTemperature();
    /** Checks if below DRIFT limit */
    boolean isBelowDriftTemperature();
    /** Checks if temperature rise is within accepted values */
    boolean correctTemperatureRise();
    
    /** Sets error code */
    void setError(enum ErrorCode e);
    /** Gets current error code */
    enum ErrorCode getError();
    
    /** Sets the current state */
    void setState(enum State s);
    /** Gets the current state */
    enum State getState();
    
    /** Sends current sensor values to serial ouptut */
    void sendCurrentValues();
    
    /** Sends debug output */
    void sendDebug(String s);
    
    
  private:
    /** Contains the current state */
    State m_state;
    
    /** Time related functions (debug serial output) */
    //Time m_t;
    
    /** Calculated PWM value of MAX temperature */
    int m_calculatedMaxTemperaturePWM;
    /** Calculated PWM value of MIN temperature */
    int m_calculatedMinTemperaturePWM;
    /** Calculated PWM value of DRIFT temperature */
    int m_calculatedDriftTemperaturePWM;
    
    /** Variable to store temperature value */
    int m_sensorValueTemperature;
    /** Variable to store temperature control potmeter value */
    int m_sensorValueTemperatureControl;
    /** Varialbe to store on/off switch value */
    int m_sensorPowerButton;
    
    /** Flag for toggle button */
    boolean m_powerButtonPushed;
    /** Flag for element power */
    boolean m_elementPowerOn;
    /** Toggle flag for power button */
    boolean m_elementPowerToggle;
    /** Flag for cooldown */
    boolean m_cooldown;
    
    /** Current error code */
    enum ErrorCode m_errorCode;
    
    /** Calculates PWM values for temperature */
    void calculatePWMValues();
    
    /** Sends status to serial output */
    void sendStatus(String s);
    
    /** Checks the temperature difference */
    int deltaTemperature();

};

//#endif // __cplusplus

#endif // APPLICATION_H
