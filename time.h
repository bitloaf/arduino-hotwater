#pragma once
#ifndef TIME_H
#define TIME_H

#include <WProgram.h>

#include "TimerOne.h"


/** Class containing time related functions */
class Time {

  public:
    /** Constuctor */
    Time();
    /** Destructor */
    ~Time();
    /** Starts the element countdown timer */
    static void startElementTimeout(int ms);
    /** Checks if the element counter has reached maximum value */
    static boolean isElementTimeout();
    /** Starts the serial countdown timer */
    static void startSerialTimeout(int ms);
    /** Checks if the serial counter has reached maximum value */
    static boolean isSerialTimeout();
    /** Starts the display countdown timer */
    static void startDisplayTimeout(int ms);
    /** Checks if the display counter has reached maximum value */
    static boolean isDisplayTimeout();
    
    /** Timer1 timeout */
    static void isr_timeout();
    
  private:
    
    /** Element timeout flag */
    static boolean m_elementTimeout;
    /** Serial timeout flag */
    static boolean m_serialTimeout;
    /** Display timeout flag */
    static boolean m_displayTimeout;
    
    /** Timeout counter */
    static int isr_counter;
    /** Element counter value */
    static int m_elementCount;
    /** Serial counter value */
    static int m_serialCount;
    /** Display counter value */
    static int m_displayCount;
    
    /** Relative Count. Returns a count value relative to the current isr_counter value */
    static int _rc(int n);
    
    /** Modulus Zero. Checks if modulus is zero */
    static boolean _mz(int n);

};


#endif // TIME_H
