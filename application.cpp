//#ifdef __cplusplus


#include "application.h"




Application::Application() {
  //init();
}

Application::~Application() {
}

void Application::init() {
  // Setup input/output and serial communication
  setupIO();
  setupSerial();
  
  // Initialize variables
  m_sensorValueTemperature = 0;
  m_sensorValueTemperatureControl = 0;
  m_sensorPowerButton = 0;
  
  // Initialize flags
  m_powerButtonPushed = false;
  m_elementPowerOn = false;
  m_cooldown = false;
  m_elementPowerToggle = false;
  
  // Calculate PWM values
  calculatePWMValues();
  
  // Initialize timer
  Time::startSerialTimeout(SERIAL_SEND_INTERVAL);
  
  // Set no errors
  setError(NO_ERRORS);
}

void Application::calculatePWMValues() {
  // PWM = 1024 * resistor ratio
  m_calculatedMaxTemperaturePWM = 1024 * (double) ( MEASURED_MAXIMUM_RESISTANCE / (MEASURED_MAXIMUM_RESISTANCE + MEASURED_R1_RESISTANCE) );
  m_calculatedMinTemperaturePWM = 1024 * (double) ( MEASURED_MINIMUM_RESISTANCE / (MEASURED_MINIMUM_RESISTANCE + MEASURED_R1_RESISTANCE) );
  double percent = DESIRED_DRIFT_TEMPERATURE / (DESIRED_MAXIMUM_TEMPERATURE - DESIRED_MINIMUM_TEMPERATURE);
  m_calculatedDriftTemperaturePWM = (double) (m_calculatedMaxTemperaturePWM - m_calculatedMinTemperaturePWM)*percent;
  
  
  m_calculatedMaxTemperaturePWM = 1023;
  m_calculatedMinTemperaturePWM = 600;
  m_calculatedDriftTemperaturePWM = 30;
  
  // Send status if debug flag set
  if (SERIAL_SEND_DEBUG_FLAG) {
    sendDebug( String(m_calculatedMaxTemperaturePWM) ); 
    sendDebug( String(m_calculatedMinTemperaturePWM) ); 
    sendDebug( String(m_calculatedDriftTemperaturePWM) ); 
  }
}

void Application::setupIO() {
  // declare the LED pin as an OUTPUT:
  pinMode(PIN_STATUS_INDICATOR, OUTPUT);  
  // initialize the pushbutton pin as an input:
  pinMode(PIN_POWER_BUTTON, INPUT);      
  // declare the relay pin as an OUTPUT:
  pinMode(PIN_RELAY_ELEMENT, OUTPUT);
}

void Application::setupSerial() {
  // Initialise serial output:
  Serial.begin(9600);
}

int Application::getTemperaturePWM() {
  return analogRead(PIN_TEMPERATURE_SENSOR);
}

int Application::getTemperatureControlPWM() {
  return analogRead(PIN_TEMPERATURE_CONTROL);
}

int Application::getTemperatureValue() {
  double t = getTemperaturePWM() - m_calculatedMinTemperaturePWM;
  t *= DESIRED_MAXIMUM_TEMPERATURE - DESIRED_MINIMUM_TEMPERATURE;
  t /= m_calculatedMaxTemperaturePWM - m_calculatedMinTemperaturePWM;
  t += DESIRED_MINIMUM_TEMPERATURE;
  return (int) t;
}

int Application::getTemperatureControlValue() {
  return getTemperatureControlPWM() / 1024;
}

boolean Application::isPowerButtonOn() {
  if ( digitalRead(PIN_POWER_BUTTON)==HIGH && !m_powerButtonPushed ) {
    m_powerButtonPushed = true;
    return true;
  }
  
  return false;
}

boolean Application::isPowerButtonOff() {
  if ( digitalRead(PIN_POWER_BUTTON)==LOW && m_powerButtonPushed ) {
    m_powerButtonPushed = false;
    return true;
  }
  
  
  return false;
}

boolean Application::getPowerButton() {
  
  int currentStatus = digitalRead(PIN_POWER_BUTTON);
  
  if ( currentStatus==LOW && m_powerButtonPushed ) {
    m_powerButtonPushed = false;
  } else if ( currentStatus==HIGH && !m_powerButtonPushed ) {
    m_powerButtonPushed = true;
    // Toggles power flag
    m_elementPowerToggle = !m_elementPowerToggle;
  }
  
  return m_elementPowerToggle;
}

void Application::waitForPowerButtonOn() {  
  // Send status if debug flag set
  if (SERIAL_SEND_DEBUG_FLAG) {
    sendDebug("WAIT POWER ON");
  }
  
  //while ( !isPowerButtonOn() );
  while ( getPowerButton()==false );
}

void Application::waitForPowerButtonOff() {  
  // Send status if debug flag set
  if (SERIAL_SEND_DEBUG_FLAG) {
    sendDebug("WAIT POWER OFF");
  }
  
  //while ( !isPowerButtonOn() );
  while ( getPowerButton()==true );
}
    
void Application::setElementPowerOn() {
  if ( !m_elementPowerOn ) {
      
    // Send status if debug flag set
    if (SERIAL_SEND_DEBUG_FLAG) {
      sendDebug("ELEMENT ON");
    }
  
    m_elementPowerOn = true;
    digitalWrite(PIN_RELAY_ELEMENT, HIGH);
  }
}

void Application::setElementPowerOff() {
  if ( m_elementPowerOn ) {
    
    // Send status if debug flag set
    if (SERIAL_SEND_DEBUG_FLAG) {
      sendDebug("ELEMENT OFF");
    }
    
    m_elementPowerOn = false;
    digitalWrite(PIN_RELAY_ELEMENT, LOW);
  }
}

boolean Application::getElementPowerOn() {
  return m_elementPowerOn;
}

void Application::setCooldown(boolean b) {
  // Send status if debug flag set
  if (SERIAL_SEND_DEBUG_FLAG) {
    sendDebug("COOLDOWN " + b ? "ON" : "OFF");
  }
  
  m_cooldown = b;
}

boolean Application::isCooldown() {
  return m_cooldown;
}

boolean Application::isHeating() {  
  // Checks if heating element has power
  if ( !getElementPowerOn() ) {
    return false;
  }
  
  // If heating power is on, and temperature below max temperature
  if ( isBelowMaxTemperature() ) {
    return true;
  }
  
  // Heating power on, but element too hot
  return false;
}

boolean Application::isAboveMaxTemperature() {
  return getTemperaturePWM() > m_calculatedMaxTemperaturePWM;
}

boolean Application::isAboveMinTemperature() {
  return getTemperaturePWM() > m_calculatedMinTemperaturePWM;
}

boolean Application::isBelowMaxTemperature() {
  return getTemperaturePWM() < m_calculatedMaxTemperaturePWM;
}

boolean Application::isBelowMinTemperature() {
  return getTemperaturePWM() < m_calculatedMinTemperaturePWM;
}

boolean Application::isBelowDriftTemperature() {
  return getTemperaturePWM() < (m_calculatedMaxTemperaturePWM - m_calculatedDriftTemperaturePWM);
}

void Application::setState(enum State s) {  
  // Send status if debug flag set
  if (SERIAL_SEND_DEBUG_FLAG) {
    sendDebug("SET STATE " + String(s) );
  }
  
  m_state = s;
}

enum State Application::getState() {
  if (SERIAL_SEND_DEBUG_FLAG) {
    sendDebug("GET STATE " + String(m_state) );
  }
  
  return m_state;
}

void Application::sendStatus(String s) {
  Serial.print("$");
  Serial.print(s);
  Serial.println("*");
}

void Application::sendCurrentValues() {
  String s = "T" + String( getTemperatureValue() );
  s += "TPWM" + String( getTemperaturePWM() );
  s += "C" + String( getTemperatureControlValue() );
  s += "CPWM" + String( getTemperatureControlPWM() );
  sendStatus(s);
}

void Application::sendDebug(String s) {
  Serial.print("**");
  Serial.print(s);
  Serial.println("**");
}

boolean Application::correctTemperatureRise() {
  return true;
}

int Application::deltaTemperature() {
}

void Application::setError(enum ErrorCode e) {
  m_errorCode = e;
}

enum ErrorCode Application::getError() {
  return m_errorCode;
}


//#endif // __cplusplus
