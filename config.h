#pragma once
#ifndef CONFIG_H
#define CONFIG_H

#ifdef __cplusplus


/** Timeout value for keeping element (in ms) */
const int ELEMENT_KEEP_TIMEOUT = 10*60*1000;
/** Timeout value for heating element (in ms) */
const int ELEMENT_HEAT_TIMEOUT = 1*60*1000;

/** Desired maximum temperature (in degrees C) */
const int DESIRED_MAXIMUM_TEMPERATURE = 90;
/** Desired minimum temperature (in degrees C) */
const int DESIRED_MINIMUM_TEMPERATURE = 10;
/** Desired drift temperature (in degrees C) */
const int DESIRED_DRIFT_TEMPERATURE = 5;

/** Measured resistance in heating element at maximum temperature (in ohm) */
const int MEASURED_MAXIMUM_RESISTANCE = 9800;
/** Measured resistance in heating element at minimum temperature (in ohm) */
const int MEASURED_MINIMUM_RESISTANCE = 1100;
/** Measured resistance in R1 (in ohm)
    See documentation for more details. */
const int MEASURED_R1_RESISTANCE = 9900;

/** Serial data send interval (in ms) */
const int SERIAL_SEND_INTERVAL = 1000;
/** Serial data debug flag */
const boolean SERIAL_SEND_DEBUG_FLAG = true;
/** Serial data status flag */
const boolean SERIAL_SEND_STATUS_FLAG = true;


#endif // __cplusplus

#endif // CONFIG_H
