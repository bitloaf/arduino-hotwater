#include "time.h"

// Initialize private static members
boolean Time::m_elementTimeout = false;
boolean Time::m_serialTimeout = false;
boolean Time::m_displayTimeout = false;
int Time::isr_counter = 0;
int Time::m_elementCount = 0;
int Time::m_serialCount = 0;
int Time::m_displayCount = 0;

Time::Time() {
  /*
  // Initialize flags
  m_elementTimeout = false;
  m_serialTimeout = false;
  m_displayTimeout = false;
  
  // Initialize timer
  isr_counter = 0;
  
  // Initialize counter values
  m_elementCount = 0;
  m_serialCount = 0;
  m_displayCount = 0;
  */
}

Time::~Time() {
}

void Time::startElementTimeout(int ms) {
  m_elementCount = _rc(ms);
  m_elementTimeout = false;
}

boolean Time::isElementTimeout() {
  // One-shot timer, does not reset flag
  return m_elementTimeout;
  //return true;
}

void Time::startSerialTimeout(int ms) {
  m_serialCount = _rc(ms);
  m_serialTimeout = false;
}

boolean Time::isSerialTimeout() {
  if (m_serialTimeout) {
    m_serialTimeout = false;
    return true;
  }
  
  return false;
}


void Time::startDisplayTimeout(int ms) {
  m_displayCount = _rc(ms);
  m_displayTimeout = false;
}

boolean Time::isDisplayTimeout() {
  if (m_displayTimeout) {
    m_displayTimeout = false;
    return true;
  }
  
  return false;
}

void Time::isr_timeout() {
  // Iterate counter
  isr_counter++;
  
  // Checks element counter
  if ( _mz(m_elementCount) ) {
    m_elementTimeout = true;
  }
  
  // Checks serial counter
  if ( _mz(m_serialCount) ) {
    m_serialTimeout = true;
  }
  
  // Checks display counter
  if ( _mz(m_displayCount) ) {
    m_displayTimeout = true;
  }
}

int Time::_rc(int n) {
  return n + (isr_counter % n);
}

boolean Time::_mz(int n) {
  return isr_counter % n == 0;
}

