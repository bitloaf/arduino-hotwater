/*
  Digitally controlled water heating
 */
 
#include "application.h"

/** Main application */
Application a;
/** Time related functions */
//Time t;
/** Display */
Display d;

/** Public wrapper for Time::isr_timeout() */
void isr_timeout();

void setup() {
  //Serial.begin(9600);
  //Serial.println("first line");
  
  // Setup input/output and serial communication
  //a.setupIO();
  //a.setupSerial();
  a.init();
  
  a.setState(IDLE);
  
  // Start timer with 1 ms
  Timer1.initialize(1000);
  
  // Attach interrupt function
  Timer1.attachInterrupt(isr_timeout);
}

void loop() {
  
//  Serial.println("next loop");
  
  switch ( a.getState() ) {
    
    // Does nothing until power button is pressed. Jumps to HEAT state.
    case (IDLE) :
      a.setElementPowerOff();
      d.setStatusIndicator(LOW);
      a.waitForPowerButtonOn();
      a.setState(HEAT);
      break;
      
    // Starts a timeout counter. If timeout or off button is pressed, application jumps to IDLE state.
    // If no interruption, element heats to max regulated temperature, application jumps to KEEP state.
    case (HEAT) :
      Time::startElementTimeout(ELEMENT_HEAT_TIMEOUT);
      a.setElementPowerOn();
      d.startBlink(1000);
      while ( a.isHeating() ) {
        // Update display
        d.update();
        
        // While heating, checks for interrupts by timeout or power button off
        //if ( t.isElementTimeout() || a.isPowerButtonOff() ) {
        if ( !a.getPowerButton() ) {
          a.setState(IDLE);
          break;
        }
        
        if ( Time::isElementTimeout() ) {
          a.setError(TIMEOUT_HEATING);
          a.setState(ERROR);
          break;
        }
        
        if ( !a.correctTemperatureRise() ) {
          a.setError(TEMPERATURE_RISE_TOO_LOW);
          a.setState(ERROR);
          break;
        }
        
      }
      // Stop blinking
      d.stopBlink();
      
      // If no interruption, heating completed
      if ( a.getState()==HEAT ) {
        a.setState(KEEP);
      }
      break;
      
    case (KEEP) :
      Time::startElementTimeout(ELEMENT_KEEP_TIMEOUT);
      a.setElementPowerOn();
      d.setStatusIndicator(HIGH);
      // While keeping, checks for interrupts by timeout or power button off
      while ( !( Time::isElementTimeout() || !a.getPowerButton() ) ) {        
        // Jumps to HEAT state if temperature too low
        if ( a.isBelowMinTemperature() ) {
          a.setState(HEAT);
          break;
        }
        
        // Send sensor values if status flag set
        if ( SERIAL_SEND_STATUS_FLAG ) { //&& m_t.isSerialTimeout() ) {
          a.sendCurrentValues();
        }
      
        // If in cooldown, turn off element. Turn off cooldown if temperature drops below DRIFT temperature
        if ( a.isCooldown() ) {
          a.setElementPowerOff();
          if ( a.isBelowDriftTemperature() ) {
            a.setCooldown(false);
          }          
        // If not in cooldown, turn on element. Turn on cooldown if temperature exceeds MAX temperature
        } else {
          a.setElementPowerOn();
          if ( a.isAboveMaxTemperature() ) {
            a.setCooldown(true);
          }
        }
      }
      // If interrupted, jumps to IDLE state
      if ( a.getState()==KEEP ) {
        a.setState(IDLE);
      }
      break;
      
    case (ERROR) :
      a.sendDebug("ERROR " + String( a.getError() ) );
      a.setElementPowerOff();
      d.startBlink(500);
      //a.waitForPowerButtonOff();
      // Wait for power button off
      while ( a.getPowerButton() ) {
        // Update display
        d.update();
      }
      // Stop blinking
      d.stopBlink();
      
      a.setState(IDLE);
      break;
      
    default:
      break;
  }
}

void isr_timeout() {
  Time::isr_timeout();
}




/******************************************************/
// Returns true if the heating element needs additional heating
/*
boolean needsHeating(int sensor) {
  // Reads from temperature sensor pin:
  //tempSensorValue = analogRead(tempSensorPin);
  // Reads from control potentiometer:
  //tempSensorValueMAX = analogRead(tempControlPin);
  
  // Checks if sensor value is ABOVE max limit:
  if (sensor > tempSensorValueMAX+tempSensorValueDRIFT) {
    return false;
  }
  
  // Checks if sensor value is BELOW min limit:
  if (sensor < tempSensorValueMIN-tempSensorValueDRIFT) {
    return true;
  }
  
  // Else, is within limits:
  return false;  
  
}

// Powers on the device:
void powerOn() {
  digitalWrite(ledPin, HIGH);
}

// Powers off the device:
void powerOff() {
  digitalWrite(ledPin, LOW);
}
*/
