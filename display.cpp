#include "display.h"

Display::Display() {
}

Display::~Display() {
}

void Display::setStatusIndicator(int s) {
  if (s == HIGH) {
    stopBlink();
    digitalWrite(PIN_STATUS_INDICATOR, HIGH);
  } else {
    digitalWrite(PIN_STATUS_INDICATOR, LOW);
  }    
}

void Display::startBlink(int ms) {
  Time::startDisplayTimeout(ms);
  isBlink = true;
}

void Display::stopBlink() {
  isBlink = false;
}

void Display::update() {
  if ( isBlink && Time::isDisplayTimeout() ) {
    toggleStatus();
  }
}

void Display::toggleStatus() {
  int s = digitalRead(PIN_STATUS_INDICATOR);
  setStatusIndicator( s==HIGH ? LOW : HIGH );
}
